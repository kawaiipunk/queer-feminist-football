# Queer Feminist Football Klub

## Background
### Why was it setup?
Queer Feminist Football Klub is an initiative that begun in Birmingham UK to create a space for marginalised working class people to enjoy the beautiful game and tackle macho culture within sports more generally. The club was first founded around the time of the 2018 Men's world cup and initially took place in [Cannon Hill Park](https://en.wikipedia.org/wiki/Cannon_Hill_Park).

### Who is it for?
This club is primarily for LGBTQIA people and also women who don't fall under the LGBT+ umbrella. Cisgender men are welcome to come and play with us but please be supportive and make room for others. All attendees are expected to abide by the club's code of conduct and generally be excellent to each other.

### What happens at a meetup?
Meetups will either involve playing football together or screening a match or some other football related activity. All decisions should be made through consensus of the group.

## The Match Rules
### Random Teams
The teams will by picked by two randomly selected team captains. The other players will then pick consecutive numbers out of the earshot of the captains. The captains will then pick their teams by calling out numbers in the range that the players selected. 

### Adjusting Teams
If the teams are imbalanced, then players can be swapped around decided by the consensus of all participants.

### Free Kicks, Fouls and Penalties
Free kicks and fouls will be awarded by the consensus of all participants. Aggressive physical contact is to be avoided.

### No Slide Tackling
This is to avoid injury.

### No High Balls
The ball must not be intentionally be kicked above the shoulder height of the shortest player unless it is a goal kick or corner. This is to prevent injuries or broken specs. A goal will only count if it is below the shoulder height of the keeper.

### Noncompetitive Score Keeping
The score for a match can be kept but should never be used to belittle the loosing side. There shall be no prizes for the victors. Nobody wins unless everybody wins.

### Breaks
Breaks can be called by any player at any time and must be respected by all other participants.

### Injuries
If someone is injured in the course of the club, the match or activity must stop until the injured played gives consent for play to resume.

### Passing is Key
It is important to make sure everyone gets posession where possible. Higher skilled players are asked to ensure that all players on their team get passed to when appropriate.

## Code of Conduct
### The Quick Version
Our club is dedicated to providing a harassment-free space for everyone, regardless of gender, gender identity and expression, age, sexual orientation, disability, physical appearance, body size, race, ethnicity, religion (or lack thereof). 

We do not tolerate harassment of club members in any form. Sexual language and imagery is not appropriate for any club venue, including screenings, workshops, parties, Twitter and other online media. Club participants violating these rules may be sanctioned or expelled from the club at the discretion of the club organisers.

### The Less Quick Version
Harassment includes offensive verbal comments related to gender, gender identity and expression, age, sexual orientation, disability, physical appearance, body size, race, ethnicity, religion, technology choices, sexual images in public spaces, deliberate intimidation, stalking, following, harassing photography or recording, sustained disruption of events, inappropriate physical contact, and unwelcome sexual attention.

Participants asked to stop any harassing behaviour are expected to comply immediately.

If a participant engages in harassing behaviour, the club organisers may take any action they deem appropriate, including warning the offender or expulsion from the club.

If you are being harassed, notice that someone else is being harassed, or have any other concerns, please contact an organiser immediately. If you can’t identify an organiser or want to bring something up at a later time, please email: **feministactionbrum at riseup.net**

Club organiser will be happy to help participants assist those experiencing harassment to feel safe for the duration of the club. We value your attendance.

We expect participants to follow these rules at club and workshop venues and club-related social events.

## Upcoming Events
Upcoming events will be announced on the brum Demosphere calendar found at:
[Brum Freed](https://brum.demosphere.eu/).

## Contribuiting to This Site
If you are not familar with using git or Gitlab, please contact **feministactionbrum at riseup.net** with any feedback or suggestions

This site is hosted on [GitLab Pages](https://georgeowell.gitlab.io/queer-feminist-football/). You can
[browse its source code](https://gitlab.com/georgeowell/queer-feminist-football/), fork it and start
using it on your projects.

For full documentation visit [mkdocs.org](http://mkdocs.org).
